CREATE DATABASE phpminicap;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    f_name VARCHAR(50) NOT NULL,
    l_name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE todos (
    id INT NOT NULL AUTO_INCREMENT,
    list VARCHAR(255) NOT NULL,
    mark BOOLEAN NOT NULL,
    user_id INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(user_id)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

INSERT INTO todos (list, mark, user_id) VALUES ("sample 1", 0, 1);