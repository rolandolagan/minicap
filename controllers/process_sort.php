<?php
    session_start();
    
    if(isset($_GET['sort'])){
        if($_GET['sort']==='done'){
            $_SESSION['sortData'] = " AND mark = 1";
        }elseif($_GET['sort']==='undone'){
            $_SESSION['sortData'] = " AND mark = 0";
        }elseif($_GET['sort']==='all'){
            $_SESSION['sortData'] = "";
        }
    }
    header('Location: '.$_SERVER['HTTP_REFERER']);
