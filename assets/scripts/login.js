const loginBtn = document.getElementById('loginBtn');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');

// Form Data Transfer? 
loginBtn.addEventListener('click', function(){

    let data = new FormData;

    data.append("email", emailInput.value);
    data.append("password", passwordInput.value);

    fetch('../../controllers/process_login.php', {
        method: "POST",
        body: data
    }).then(function(response){
               return response.text();
    }).then(function(response_from_fetch){
        if(response_from_fetch === "email or password is empty"){
            // toastr['error']("email or password is empty");
            document.querySelector('#err-msg').textContent = "email or password is empty";
        }else if(response_from_fetch === "wrong email or password"){
            // toastr['error']("wrong email or password");
            document.querySelector('#err-msg').textContent = "wrong email or password";
        }else if(response_from_fetch === "error"){
            // toastr['error']("error");
            document.querySelector('#err-msg').textContent = "error";
        }else{
            window.location.replace("../../home.php");
        }
    })
})