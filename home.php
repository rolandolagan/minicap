<?php    
    require 'inc/temp.php';

    function getTitle()
    {
        echo 'Home';
    }
    function getContent()
    {
        require 'connection/database.php';
        if(isset($_SESSION['user']))
        {
            $userId = $_SESSION['user']['id'];
?>

<div class="vcss-container container col-lg-5">
    <div class="vcss-menu card-body">
        <div class="vcss-left-menu">
            <a href="controllers/process_sort.php?sort=done">
                <img class="vcss-icon" src="../assets/images/icons/showdone.svg" alt="" title="Done">
            </a>
            <a href="controllers/process_sort.php?sort=undone">
                <img class="vcss-icon" src="../assets/images/icons/showundone.svg" alt="" title="Not done">
            </a>
            <a href="controllers/process_sort.php?sort=all">
                <img class="vcss-icon" src="../assets/images/icons/showall.svg" alt="" title="Show all">
            </a>
        </div>
        <!-- Button trigger modal -->
        <a type="button" data-toggle="modal" data-target="#addNewTodo">
            <img class="vcss-icon" src="../assets/images/icons/plus.svg" alt="" title="Add new">
        </a>
    </div>
    <table class="vcss-table table">
        <thead>
            <tr>
                <th class="vcss-th1" >List</th>
                <th class="vcss-th2" >Mark</th>
                <th class="vcss-th3" >Action</th>
            </tr>
        </thead>
    </table>
    <div class="vcss-table-scroll ">
        <table class="table">
            <tbody>
                <?php
                    $qry = "SELECT * FROM todos WHERE user_id = $userId";
                    if(isset($_SESSION['sortData'])){
                        $qry .= $_SESSION['sortData'];
                    }
                    $todos = mysqli_query($conn, $qry);
                    foreach($todos as $todo)
                    {
                ?>
                <tr>
                    <td class="vcss-td1"><?= $todo['list'];?></td>
                    <td class="vcss-td2">
                        <?php
                            if($todo['mark'])
                            {
                        ?>
                                <a href="controllers/process_mark.php?todo_id=<?= $todo['id'];?>">
                                    <img class="vcss-icon" src="../assets/images/icons/done.svg" alt="">
                                </a>
                        <?php
                            }else if(!$todo['mark'])
                            {
                        ?> 
                                <a href="controllers/process_mark.php?todo_id=<?= $todo['id'];?>">
                                    <img class="vcss-icon" src="../assets/images/icons/undone.svg" alt="">
                                </a>
                        <?php    
                            }
                        ?>
                    </td>
                    <td class="vcss-td3">
                        <a href="controllers/process_delete.php?todo_id=<?= $todo['id'];?>">
                            <img class="vcss-icon" src="../assets/images/icons/delete.svg" alt="">
                        </a> <span> | </span>
                        <a class="editBtn" href="edit.php?todo_id=<?= $todo['id'];?>">
                                <img class="vcss-icon" src="../assets/images/icons/edit.svg" alt="">
                        </a>
                    </td>
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    
    <!-- ADD TODO Modal -->
    <div id="addNewTodo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New Todo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="../controllers/process_add.php" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="user_id" value="<?= $userId;?>" >
                        <input class="form-control" type="text" name="todo_list" aria-describedby="emailHelp">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="add_todo" class="btn btn-success">Ok</button>
                </div>
            </form>
            </div>
        </div>
    </div>
    <script>
        
    </script>
    <!-- EDIT TODO Modal -->
    <!-- <div id="editTodo" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Todo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <input id="list-id" type="number" name="todo_id" value="">
                        <input id="list-data" class="form-control" type="text" name="todo_list" value="" aria-describedby="emailHelp">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="add_todo" class="btn btn-success">Ok</button>
                </div>
            </form>
            </div>
        </div>
    </div> -->
    
</div>
<!-- end bracket of top most php -->
<?php 
        }    
    }
?>