<!-- Login JS -->
<script src="../../assets/scripts/login.js" defer></script>

<?php
    require 'inc/temp.php';
    
    function getTitle()
    {
        echo 'Login';
    }
    function getContent()
    {
?>
<!-- --------------------------------------- -->
<div class="vcss-login container">
    <div class="vcss-log-wrapper">
        <h1>Login</h1>
        <form action='controllers/process_login.php' method="POST">
        <label for="email">Email:</label>
            <input id="email" class="form-control" type="text" name="email" placeholder="Enter email...">
            <label for="password">Password:</label>
            <input id="password" class="form-control" type="password" name="password" placeholder="Enter password...">
            <span id="err-msg" class="vcss-err-msg text-danger"></span>
            <button class="vcss-log-btn btn mt-3" type="button" id="loginBtn">Login</button>
        </form>
        <div class="bot-note"> 
            <h6 class="mt-4">Not yet registered? <a href="register.php">Sign up</a></h5>
        </div>
    </div>
</div>

<?php
    }
?>