<div class="vcss-main">
    <header class="vcss-header">
        <nav class="vcss-nav">
            <?php if(isset($_SESSION['user'])){ ?>
            <div class="nav-div1">
                <h3>
                    <a href="../controllers/process_sort.php?sort=all">
                        <img class="vcss-logo" src="../assets/images/icons/todo.svg" alt="">
                        <span>Rollin Do's</span>
                    </a>
                </h3>
            </div>
            <div class="nav-div2">
                <h6>Hi <?= $_SESSION['user']['f_name']; ?> ! | 
                    <a href="../controllers/process_logout.php">
                        <img class="vcss-icon" src="../assets/images/icons/logout.svg" alt="">
                    </a>
                </h6>
            </div>
            <?php }else
            {
            ?>
            <div class="nav-div3">
                <h3><img class="vcss-logo" src="../assets/images/icons/todo.svg" alt="">Rollin Do's</h3>
            </div>
            <?php
            } ?>
        </nav>
    </header>